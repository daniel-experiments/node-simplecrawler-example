/**
 * An experiment that implements simplcrawler
 */

require( 'dotenv' ).config();

const Crawler         = require( 'simplecrawler' );
const cheerio         = require( 'cheerio' );
const excerptHtml     = require( 'excerpt-html' );
const { detergent }   = require( 'detergent' );
const cleanTextUtils  = require( 'clean-text-utils' );
const filesize        = require( 'filesize' );
const md5             = require( 'md5' );
const sh              = require( 'shorthash' );
const url             = require( 'url' );
const AWS             = require( 'aws-sdk' );
const pdf             = require( 'pdf-parse' );

var domains = require( './domains.json' ); // Domain whitelist
var crawler = new Crawler( process.env.SC_CRAWL_URL );
var pkg = require('./package.json');

/**
 * Crawler configuration
 */
crawler.userAgent = 'Mozilla/5.0 (compatible; simplecrawler/' + pkg.dependencies.simplecrawler.replace( /[^\d.]/g, '' ) + '; +https://github.com/simplecrawler/simplecrawler)';
crawler.interval = process.env.SC_CRAWL_INTERVAL; // in milliseconds
crawler.maxConcurrency = process.env.SC_CRAWL_MAX_CONCURRENCY; // Maximum concurrent requests
crawler.maxDepth = process.env.SC_CRAWL_DEPTH; // Max page depth. Set to 1 for front page only
crawler.scanSubdomains = true;
crawler.decodeResponses = true;
crawler.downloadUnsupported = false; // Set to true to process PDF, etc
crawler.supportedMimeTypes = [ /^text\/html/, /^application\/pdf/ ]; // Process HTML & PDF files only
crawler.stripQuerystring = true;
crawler.ignoreWWWDomain = true;
crawler.domainWhitelist = domains.whitelist;
//crawler.respectRobotsTxt = false;

// excerptHtml options
var excerptConfig = {
  stripTags:   true, // Set to false to get html code
  pruneLength: process.env.SC_EXCERPT_LENGTH, // Amount of characters that the excerpt should contain
  //pruneString: '&hellip;', // Character that will be added to the pruned string
  pruneString: '',
  pruneSeparator: ' ', // Separator to be used to separate words
};

// detergent options
var detergentConfig = {
  removeWidows: false,
  replaceLineBreaks: false,
  removeLineBreaks: true,
  convertDotsToEllipsis: false,
  keepBoldEtc: false,
  convertApostrophes: false,
  convertEntities: false // Encode HTML characters
};

/**
 * Event handler when page fetched
 */
crawler.on( 'fetchcomplete', function( queueItem, responseBuffer, response ) {

  var $ = cheerio.load( responseBuffer );

  // Important response data
  var response_code   = queueItem.code;
  var response_type   = response.headers['content-type'];

  // Page body (inside element with 'content' ID)
  var body = $( '#content' ).length ? $( '#content' ) : $( 'body' );

  // Remove specific elements. Example: Remove H1 header in traditional WordPress pages:
  body.find( 'h1.entry-title' ).remove();

  // Replace "smart" quotes (Ex: https://practicaltypography.com/straight-and-curly-quotes.html)
  var content_text = cleanTextUtils.replace.smartChars( body.text() );

  // Normalize whitespace and convert HTML elements
  var content = detergent( content_text, detergentConfig ).res;

  // Set content variables
  var page_url        = queueItem.url;
  var page_title      = $( 'title' ).text().trim();
  var page_size       = responseBuffer.length;
  var page_size_round = filesize( page_size, { base: 10, round: 0 } );
  var content_excerpt = excerptHtml( content, excerptConfig );

  console.log( 'URL: ' + page_url );
  console.log( 'Title: ' + page_title );
  console.log( 'Page Size: ' + page_size_round );
  console.log( 'Content Type: ' + response_type );
  console.log();
  console.log( content_excerpt ); // Content excerpt
  //console.log( body.html() ); // Raw HTML
  //console.log( body.text() ); // With smart quotes
  //console.log( content_text ); // Without smart quotes
  //console.log( content ); // Whitespace normalized
  console.log( '---' );

  /**
   * Insert code to insert/update data in search index
   */

  /**
   * AWS CloudSearch example
   */

 var csd = new AWS.CloudSearchDomain( { region: '{insert_region}', endpoint: '{insert_enpoint.cloudsearch.amazonaws.com}' } );
 var payload = [];

 // Parse URL and set variables
 var parsed_url = url.parse( page_url );
 var link_slug = parsed_url.hostname + parsed_url.pathname;

 // Create document object
 var result = {
   "type": "add",
   "id": parsed_url.hostname + '-' + sh.unique( link_slug ),
   "fields": {
     "url": parsed_url.href,
     "title": page_title,
     "domain": parsed_url.hostname,
     "content_type": response_type.split( ";" )[0],
     "page_size": page_size_round.replace( /\D/g, '' ), //page_size_round 12 kB
     "category": [ "" ],
     "tags": [ "" ],
     "content": content,
     "locale": locale ? switch_lang( locale ) : 'en_US'
   }
 };

 // Create payload
 payload.push( result );

 var params = {
   contentType: 'application/json',
   documents: JSON.stringify( payload )
 };

 // Upload to CloudSearch
 csd.uploadDocuments( params, function( err, data ) {
   if ( err ) console.log( err, err.stack ); // an error occurred
   else console.log( data ); // successful response
 });

});

crawler.start();

/* Helper Functions */

function switch_lang( lang ) {

  lang = lang.replace( '-', '_' );

  switch( lang.toLowerCase() ) {
      case 'en':
        return 'en_US';
        break;
      case 'es':
        return 'es_ES';
        break;
      default:
        return lang;

  }

}
