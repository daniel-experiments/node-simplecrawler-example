[![Author](https://img.shields.io/badge/author-Daniel%20M.%20Hendricks-lightgrey.svg?colorB=9900cc )](https://www.danhendricks.com)
[![License](https://img.shields.io/badge/license-Apache-yellow.svg)](https://gitlab.com/daniel-experiments/node-simplecrawler-example/blob/master/LICENSE)
[![Twitter](https://img.shields.io/twitter/url/https/gitlab.com/daniel-experiments/node-simplecrawler-example.svg?style=social)](https://twitter.com/danielhendricks)

# Node.js Simplecrawler Usage Example

This is an example of using [simplecrawler](https://github.com/simplecrawler/simplecrawler) to crawl a web site.

### AWS Credentials

In order for this to work, you must add your AWS credentials to `~/.aws/credentials`. Example:

```
[default]
aws_access_key_id = XXXXXXXXXXXXXXXXXXXX
aws_secret_access_key = XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```

### Usage

1. Edit the variables in `.env` and `domains.json` (additional whitelisted domains).
1. Load dependencies: `npm install`
1. To execute: `npm run start`

### Misc Notes

If you want to delete all documents from your index, this script works well:
[https://gist.github.com/jthomerson/ca06245d316d485252579a7d42630095](https://gist.github.com/jthomerson/ca06245d316d485252579a7d42630095)

**Example:**
```
./delete-all-cloudsearch-documents.sh --doc-domain https://doc-xxxxxxxxx.cloudsearch.amazonaws.com
```
### CloudSearch Fields

These are the fields that I'm using in CloudSearch for the example:

![Example Fields](https://gitlab.com/daniel-experiments/node-simplecrawler-example/raw/master/assets/screenshot-1.png "Example Fields")
